<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fun Shop | Welcome</title>
    <link rel="stylesheet" href="./css/style.css">

</head>

<body>

    <header>
        <div class="container">
            <div id="branding">
                <h1>
                    <span class="highlight">Fun Shop</span>
                </h1>
                </div>
                <div id="shoppingcart">
                        <a href="cart.php">
                                <img src="img/Shopping.png" width="40" height ="30">
                </div>
            <nav>
                <ul>

                     <li class="current">
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="products.php">Products</a>
                    </li>
                    <li>
                        <a href="">SALES!!!</a>
                    </li>
                    <li>
                        <a href="">My Account</a>
                    </li>
                    <li>
                        <a href="">Contact</a>
                    </li>
                    <li>
                        <a href="">About</a>
                    </li>
					<li>
                        <a href="login.php">login</a>
                    </li>


                </ul>
            </nav>
        </div>


    </header>




    <Section id="showcase">
        <div class="container">
            <h1>Fun Shop | Funny and Friendly </h1>
        </div>
    </Section>

<hr>

    <Section id="boxes">
        <h1>FEATURED PRODUCTS</h1>
        <div class="container">
            <div class="box">
                <img src="img/Converse.png">
                <h3>Converse X Hello Kitty One Star Junior Low Top Prism Pink</h3>
                <p>Price: $89.99</p>

            </div>
            <div class="box">
                <img src="img/converse2.png">
                <h3>Chuck Taylor All Star Classic Colour High Top Black</h3>
                <p>Price: $99.99</p>
            </div>
            <div class="box">
                <img src="img/converse3.png">
                <h3>Converse X Hello Kitty Chuck Taylor All Star Low Top Black</h3>
                <p>Price: $119.99</p>
            </div>
            <div class="box">
                <img src="img/converse4.png">
                <h3>Converse X Comme des Gar�ons Chuck Taylor All Star 70 Play High Top Black</h3>
                <p>Price: $140.00</p>
            </div>
            <div class="box">
                <img src="img/converse5.png">
                <h3>Chuck Taylor All Star Classic Colour Low Top White</h3>
                <p>Price: $99.99</p>
            </div>
            <div class="box">
                <img src="img/converse6.png">
                <h3>El Distrito Low Top White</h3>
                <p>Price: $90.00</p>
            </div>


    </Section>

    <Footer>
        <p> Chuong Pham Lam, s3660068 </p>
      <div>Disclaimer: This website is not a real website and is being developed as part of a School of Science Web Programming course at RMIT University in Melbourne, Australia.</div>

    </Footer>
</body>

</html>
