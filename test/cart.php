<?php
    
if(isset($_POST["add_to_cart"]))
    {
    if(isset($_SESSION["shopping_cart"]))
	{
		$item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
		if(!in_array($_GET["id"], $item_array_id))
		{
			$count = count($_SESSION["shopping_cart"]);
			$item_array = array(
				'item_id'			=>	$_GET["id"],
                'size'              =>  $_POST["item_size"],
				'item_name'			=>	$_POST["hidden_name"],
				'item_price'		=>	$_POST["hidden_price"],
				'item_quantity'		=>	$_POST["qty"]
			);
			$_SESSION["shopping_cart"][$count] = $item_array;
    
		}
		else
		{
			echo '<script>alert("Item Already Added")</script>';
		}
	}
	else
	{
		$item_array = array(
			'item_id'			=>	$_GET["id"],
            'size'              =>  $_POST["item_size"],
			'item_name'			=>	$_POST["hidden_name"],
			'item_price'		=>	$_POST["hidden_price"],
			'item_quantity'		=>	$_POST["qty"]
		);
		$_SESSION["shopping_cart"][0] = $item_array;
	}
}
if(isset($_GET["action"]))
{
	if($_GET["action"] == "delete")
	{
		foreach($_SESSION["shopping_cart"] as $keys => $values)
		{
			if($values["item_id"] == $_GET["id"])
			{
				unset($_SESSION["shopping_cart"][$keys]);
				echo '<script>alert("Item Removed")</script>';
				
			}
		}
	}
}
 if(isset($_POST['update'])){
			$count = count($_SESSION["shopping_cart"]);

        for ($x=0; $x<$count; $x++) {
        $_SESSION["shopping_cart"][$x]['item_quantity'] = $_POST['num'][$x];

}


 }

if (isset($_POST['cancel'])) {
  unset($_SESSION['shopping_cart']);
  header("Location: cart.php");
}
    
?>


    <main>
    	<h3>Order Details</h3>
			<form class="" action="cart.php?action=submit&id=0" method="post"><div class="table-responsive">
				<table class="table table-bordered">
					<tr>
						<th width="30%">Item Name</th>
                        <th width="10%">Size  </th>
						<th width="10%">Quantity</th>
						<th class='price' width="20%">Price</th>
						<th class='sum' width="15%">Total</th>
						<th width="5%">Action</th>
					</tr>
					<?php
					if(!empty($_SESSION["shopping_cart"]))
					{
						$total = 0;
						foreach($_SESSION["shopping_cart"] as $keys => $values)
						{
					?>
					<tr>
						<td><?php echo $values["item_name"]; ?></td>
                        <td><?php echo $values["size"]; ?></td>
						<td>
        <input type="number" id="qty" name="num[]" value=<?php echo $values["item_quantity"]?>>

      </td>
						<td class='price'>$ <?php echo $values["item_price"]; ?></td>
						<td class='sum'>$ <?php echo number_format($values["item_quantity"] * $values["item_price"], 2);?></td>
						<td><a href="cart.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Remove</span></a></td>
					</tr>
					<?php
							$total = $total + ($values["item_quantity"] * $values["item_price"]);
						}
					?>
					<tr>
						<td id='total' colspan="3" align="right">Total</td>
						<td id='total' align="right">$ <?php echo number_format($total, 2); ?></td>
						<td></td>
					</tr>
					<?php
					}
					?>
						
				</table>
			</div>
            <div style="margin: 50px;"> <input type="button" class="button-submit" name="continue-shopping" value="Continue Shopping" onclick="location.href = 'products.php'">
                    <input type="button" class="button-submit" name="check-out" value="Check out" onclick="location.href = 'checkout.php'">
                        <input type="submit" class="button-submit" name="cancel" value="cancel" onclick="location.href = 'cart.php'">
                <input type="submit" class="button-submit" name="update" value="update" onclick="location.href = 'cart.php'">
           </div>
        </form>
    </main>





<?php   
    require_once("tools.php");
    end_module();
?>
