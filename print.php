<?php
session_start();
include "header.php";

if (empty($_SESSION["shopping_cart"]) || empty($_SESSION["info"])) {
    header("Location: error.php");
}
$str = file_get_contents('product.json');

$json = json_decode($str, true);

$products = $json['products'];

$result = [];

foreach ($_SESSION["shopping_cart"] as $key => $value) {
    foreach ($products as $k => $v) {
        if ($value['id'] == $v['id']) {
            $result[] = array_merge($v, $value);
        }
    }
}

$strW = '';
foreach ($result as $r) {
    $strW .= "Name:{$r['name']} - size:{$r['size']} - quantity:{$r['quantity']} - price:{$r['price']} |";
}

$strInfo = '';
if (isset($_SESSION["info"])) {
    $strInfo .="Name:".$_SESSION["info"]["fullname"].' - Email :'.$_SESSION["info"]["email"].' - Phone:'.$_SESSION["info"]["phone"]. ' - Credit cart'.$_SESSION["info"]["credit_cart"].'|';
}

?>


<main>
    <h3>Order Details</h3>
    <form action="cart.php" method="post"><div class="table-cart">
            <table class="table-cart">
                <tr>
                    <th width="30%">Item Name</th>
                    <th width="10%">Size  </th>
                    <th width="10%">Quantity</th>
                    <th class='price' width="20%">Price</th>

                    <th class='sum' width="15%">Total</th>
                    <th width="5%">Action</th>
                </tr>
                <?php
                if(count($result) > 0)
                {
                    $total = 0;
                    foreach($result as $keys => $values)
                    {
                        ?>
                        <tr>
                            <td><?php echo $values["name"]; ?></td>
                            <td><?php echo $values["size"]; ?></td>
                            <td><input type="number" id="qty" name="num[]" value=<?php echo $values["quantity"]?>></td>
                            <td class='price'>$ <?php echo $values["price"]; ?></td>
                            <td class='sum' >$ <?php echo number_format($values["quantity"] * $values["price"], 2 );?></td>
                            <td><a href="cart.php?action=delete&id=<?php echo $values["id"]; ?>"><span class="text-danger">Remove</span></a></td>
                        </tr>
                        <?php
                        $total = $total + ($values["quantity"] * $values["price"]);
                    }
                    ?>
                    <tr>
                        <td id='total' colspan="3" align="right">Total</td>
                        <td id='total' align="right">$ <?php echo number_format($total, 2); ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                }
                ?>

            </table>

        </div>

    </form>
    <table class="table-cart">
        <tr>
            <th width="30%">Name</th>
            <th width="10%">Email </th>
            <th width="10%">Phone</th>
            <th class='price' width="20%">Credit cart</th>
            <th class='sum' width="15%">Expiry day</th>
            <th></th>
        </tr>
        <?php
        if(isset($_SESSION["info"]))
        {
                ?>
                <tr>
                    <td><?php echo $_SESSION["info"]["fullname"]; ?></td>
                    <td><?php echo $_SESSION["info"]["email"]; ?></td>
                    <td><?php echo $_SESSION["info"]["phone"]; ?></td>
                    <td><?php echo $_SESSION["info"]["credit_cart"]; ?></td>
                    <td><?php echo $_SESSION["info"]["expiry_day"]; ?></td>
                    <td><form>
                            <input type="button" value="Print" style="font-size: 20px; padding: 10px;" onclick="saveData()" />
                        </form></td>
                </tr>
            <?php
        }
        ?>

    </table>

</main>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    $(document).ready(function () {
      saveData = () => {
        $.ajax({
          url: "createData.php",
          method: "POST",
          data: { 'product' : "<?php echo $strW; ?>", 'info': "<?php echo $strInfo; ?>" },
          dataType: "html"
        }).done(function() {
          window.print()
        }).fail(function() {
          alert( "error" );
        })

      }
    })
</script>
<?php include "footer.php";?>

